<?php

namespace Drupal\schema_dataset\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'Dataset' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_dataset",
 *   label = @Translation("Schema.org: Dataset"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>. Also see <a href="":url2"">Google's requirements</a>.", arguments = {
 *     ":url" = "https://schema.org/Dataset",
 *     ":url2" = "https://developers.google.com/search/docs/data-types/dataset",
 *   }),
 *   weight = 10,
 * )
 */
class SchemaDataset extends SchemaGroupBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
