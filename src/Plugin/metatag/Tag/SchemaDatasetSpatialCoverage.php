<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_spatial_coverage' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_spatial_coverage",
 *   label = @Translation("spatial coverage"),
 *   description = @Translation("RECOMMENDED BY GOOGLE"),
 *   name = "spatialCoverage",
 *   group = "schema_dataset",
 *   weight = 5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "geo_coordinates",
 *   tree_parent = {
 *     "GeoCoordinates",
 *   },
 *   tree_depth = 0
 * )
 */
class SchemaDatasetSpatialCoverage extends SchemaNameBase {


}
