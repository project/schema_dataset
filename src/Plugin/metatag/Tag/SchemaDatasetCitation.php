<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_citation' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_dataset_citation",
 *   label = @Translation("citation"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. Identifies academic articles that are recommended by the data provider be cited in addition to the dataset itself."),
 *   name = "citation",
 *   group = "schema_dataset",
 *   weight = 1,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "text",
  *  tree_parent = {},
  *  tree_depth = -1
 * )
 */
class SchemaDatasetCitation extends SchemaNameBase {

}
