<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_creator' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_dataset_creator",
 *   label = @Translation("creator"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. Creator of the dataset"),
 *   name = "creator",
 *   group = "schema_dataset",
 *   weight = 1,
 *   type = "string",
 *   property_type = "organization",
 *   tree_parent = {
 *     "Person",
 *     "Organization",
 *   },
 *   tree_depth = 0,
 *   secure = FALSE,
 *   multiple = TRUE
 * )
 */
class SchemaDatasetCreator extends SchemaNameBase {

}
