<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_sameas' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element identifier.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_dataset_sameas",
 *   label = @Translation("sameAs"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. URL of a reference Web page that unambiguously indicates the dataset's identity, usually in a different repository."),
 *   name = "sameAs",
 *   group = "schema_dataset",
 *   weight = 5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1
 * )
 */
class SchemaDatasetSameAs extends SchemaNameBase {

}
