<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_name' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_dataset_name",
 *   label = @Translation("name"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. A descriptive name of a dataset. For example, &ldquo;Snow depth in Northern Hemispher&rdquo;."),
 *   name = "name",
 *   group = "schema_dataset",
 *   weight = 1,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1
 * )
 */
class SchemaDatasetName extends SchemaNameBase {

}
