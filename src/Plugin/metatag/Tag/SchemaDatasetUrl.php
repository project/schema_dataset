<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_url' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_dataset_url",
 *   label = @Translation("url"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. Location of a page describing the dataset"),
 *   name = "url",
 *   group = "schema_dataset",
 *   weight = 3,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1
 * )
 */
class SchemaDatasetUrl extends SchemaNameBase {

}
