<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_alternate_name' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element identifier.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_dataset_alternetname",
 *   label = @Translation("Alternate Name"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. Alternative names that have been used to refer to this dataset, such as aliases or abbreviations. Example (in JSON-LD format):"),
 *   name = "alternateName",
 *   group = "schema_dataset",
 *   weight = 5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1
 * )
 */
class SchemaDatasetAlternateName extends SchemaNameBase {

}
