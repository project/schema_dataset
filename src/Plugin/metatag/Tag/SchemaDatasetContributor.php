<?php

namespace Drupal\schema_dataset\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_dataset_contributor' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_dataset_contributor",
 *   label = @Translation("contributor"),
 *   description = @Translation("Contributor to the dataset"),
 *   name = "contributor",
 *   group = "schema_dataset",
 *   weight = 1,
 *   type = "string",
 *   property_type = "organization",
 *   tree_parent = {
 *     "Person",
 *     "Organization",
 *   },
 *   tree_depth = 0,
 *   secure = FALSE,
 *   multiple = TRUE
 * )
 */
class SchemaDatasetContributor extends SchemaNameBase {

}
