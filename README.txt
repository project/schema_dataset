Schema.org Metatag Dataset
--------------------------------------------------------------------------------
This module extends Schema.org Metatag module to display structured data
representing scientific datasets as JSON-LD in the head of web pages.

See https://schema.org/Dataset for more info about Datasets metadata.


Maintainers
------------------------------------------------------------------------------

Alexander O'Neill - https://drupal.org/u/alxp

Development sponsored by UPEI Robertson Library https://library.upei.ca/

Thanks to funding by CANARIE https://www.canarie.ca/